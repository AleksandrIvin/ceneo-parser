<?php

namespace ivin\parser\tests;

use ivin\parser\CeneoParser;

/**
 * Description of CeneoParserTest
 *
 * @author Aleksandr Ivin <aleksandr.ivin.ua@gmail.com>
 */
class CeneoParserTest extends \PHPUnit_Framework_TestCase {
    
    public function testCreate() {
        
        $this->assertObjectHasAttribute('text', new CeneoParser()); 
        $this->assertObjectHasAttribute('_client', new CeneoParser()); 
        
    }
    
    /**
     * @dataProvider textDataProvider
     * @param type $text
     */
    public function testResponse($text) {
        $cp = new CeneoParser($text);
        $cp->parse();
        print_r($cp->items);
        print_r($cp->errors);
        print_r($cp->messages);
        sleep(2);
        
    }
    
    public function textDataProvider() {
        return [
            ['cbdh eheduf ssjdjf'],
            ['SAMSUNG UE-32J5100'],
            ['SAMSUNG UE-40JU6400'],
            ['BRAUN K700'],
            ['ZELMER 919.0ST']
        ];
    }
}
