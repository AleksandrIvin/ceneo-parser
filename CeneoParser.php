<?php

namespace ivin\parser;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Description of CeneoParser
 *
 * @author Aleksandr Ivin <aleksandr.ivin.ua@gmail.com>
 */
class CeneoParser {
    
    const SEARH_URL = 'http://www.ceneo.pl/search';
    
    const NOT_FOUND_MARKER_SMALL_ALERT = 'Ponieważ nie mogliśmy znaleźć';

    public $errors;
    public $messages;
    public $items;
    
    public $text;
    
    private $_client;
    private $_response;
    
    public function __construct($text = null) {
        $this->setText($text);
        $this->_client = new Client([
            'base_uri' => self::SEARH_URL,
            'cookies' => true
            ]);
    }
    
    /**
     * 
     * @return array|false
     */
    
    public function parse() {
        $this->sendRequest();
        $htmlBody = $this->getHtml();
        
        if($this->findAlert($htmlBody)) {
            $this->errors[] = 'Alert!';
            return false;
        }
                
        $this->items = $this->findItems($htmlBody);
        return $this->items;
    }
    
    /**
     * 
     * @return string
     */
    public function getHtml() {
        return $this->_response->getBody();
    }
    
    /**
     * 
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }
    
    
    /**
     * 
     * @return Client
     */
    
    private function sendRequest() {
        $this->_response = $this->_client->post(self::SEARH_URL, [
            'form_params' => [
                'category_id' => '',
                'search-query' => $this->text,
            ]
        ]);
        
        return $this->_response;
    }
    
    /**
     * Find alert on page. Big alert h3 Not Found, and small found other models
     * 
     * @param string $htmlBody
     * @return boolean
     */
    private function findAlert($htmlBody) {
        $crawler = new Crawler;
        $crawler->addContent($htmlBody);
        
        if(($alert = $crawler->filter('.alert > h3') ) && $alert->count()) {
            $this->messages[] = $alert->text();
            return TRUE;
        }
        
        // @todo refactor this
        if (($alert = $crawler->filter('.alert'))
                && $alert->count()
                && strpos($alert->text(), self::NOT_FOUND_MARKER_SMALL_ALERT)) {
            $this->messages[] = $alert->text();
            return TRUE;
        }

        return FALSE;
    }
    
    /**
     * 
     * @param string $htmlBody
     * @return array
     */
    
    private function findItems($htmlBody) {
        $crawler = new Crawler;
        $crawler->addContent($htmlBody);
        $items = NULL;
                
        $rows = $crawler->filter('.category-list-body .cat-prod-row');
        foreach($rows as $row) { 
            $newRow = $this->parseItem($row);
            
            if(isset($newRow)) { 
                $items[] = $newRow;
                unset($newRow);
            }
        }
        
        return $items;        
    }
    
    
    /**
     * 
     * @param DOMNode $item
     * @return array
     */
    
    private function parseItem($item) {
        //$itemArr = NULL;
        
        $crawler = new Crawler; 
        $crawler->addNode($item);
        //print_r($crawler);
                
        $itemArr = [
            'name' => null,
            'rating' => null,
            'opinions' => null,
            'rating' => null,
            'category_name' => null,
            'votes' => null,
            'price' => null,
            'shops' => null
        ];
        
        if(($nodes = $crawler->filter('.cat-prod-row-name')) && $nodes->count()) {
            $itemArr['name'] = trim($nodes->first()->text());
        }
        
        if(($nodes = $crawler->filter('.product-score')) && $nodes->count()) {            
            $itemArr['rating'] = (float)substr(trim($nodes->first()->text()),0,4);
        }
                
        if(($nodes = $crawler->filter('.cat-prod-row-category > a')) && $nodes->count()) {            
            $itemArr['category_name'] = trim($nodes->first()->text());
        }
        
        if(($nodes = $crawler->filter('.product-reviews-link__votes-count')) && $nodes->count()) {            
            $itemArr['votes'] = (int)trim($nodes->first()->text());
        }
        
        if(($nodes = $crawler->filter('.product-reviews-link')) && $nodes->count()) {            
            $itemArr['opinions'] = $this->pickupOpinions($nodes->first()->text());
        }
        
        if(($nodes = $crawler->filter('span.price > span.value')) && $nodes->count()) {
            $itemArr['price_brutto'] = $this->toFloat(trim($nodes->first()->text()));
        }
        
        if(($nodes = $crawler->filter('.shop-numb')) && $nodes->count()) {            
            $itemArr['shops'] = $this->pickupShops($nodes->first()->text());
        }
                
        return $itemArr;
    }
    
    
    /**
     * 
     * @param string $text
     * @return boolean
     */
    
    private function pickupShops($text) {
        if(preg_match('/([0-9]+)/', $text, $match)) {
            return (int)$match[0];
        }
        
        return FALSE;
    }
    
    /**
     * 
     * @param string $text
     * @return boolean
     */
    private function pickupOpinions($text) {
        if(preg_match('/(([0-9]+)\sopin)/', $text, $match)) {
            return (int)$match[2];
        }
        
        return FALSE;
    }
    
    /**
     * 
     * @param string $val
     * @return float
     */
    private function toFloat($val) {
        return floatval(str_replace(',', '.', $val));
    }
    
}
